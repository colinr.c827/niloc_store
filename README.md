# Niloc's eCommerce Cube Store

Niloc's Store is a fictional eCommerce Store that sells Rubik's cubes. 

## Built Using:
* Ruby 2.5.1
* Rails 5.2.1

## Gems Used:
* [Bulma Rails](https://github.com/jgthms/bulma)
* [Simple Form](https://github.com/plataformatec/simple_form)
* [Devise](https://github.com/plataformatec/devise) 
* [Gravatar](https://github.com/mdeering/gravatar_image_tag)
* [Mini Magick](https://github.com/minimagick/minimagick)
* [CarrierWave](https://github.com/carrierwaveuploader/carrierwave)

## Installation Instructions

1. Install dependencies via `bundle install`
2. Migrate database `rails db:migrate`
3. Seed database `rails db:seed`
4. You're all set!

